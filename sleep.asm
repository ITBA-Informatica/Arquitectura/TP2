section .text

global _start
extern printInt
extern printN
extern exit

_start:

push start
call printN

mov eax,0a2h
mov ebx,delay
int 80h
push end
call printN
call exit

section .data
start db "Start!", 0
end db "Finished", 0
delay dd 5,0