section .text
GLOBAL multiples
extern _print
extern castIntToString

multiples:
	pop eax ; return address
	pop ebx; int k
	pop ecx; int n
	push eax
	push ecx
	push ebx
	; Stack = return address | n | k
ciclo:
; 
	; pop eax
	; push eax
	; push eax
	; call castIntToString
	; call _print
; 

	mov edx,0
	pop eax; k
	pop ecx; n
	push ecx
	push eax
	; Stack Address | n | k
	div ecx
	cmp edx,0
	jg next
	; Print de k
	pop eax
	push eax
	push eax
	call castIntToString
	call _print
next:
	; Stack Address | n | k
	pop ebx; k
	pop eax; n
	dec ebx; k = k-1
	jz return
	push eax
	push ebx
	jmp ciclo
return:
	ret

