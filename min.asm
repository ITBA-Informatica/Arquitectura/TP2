section .text

global min
extern _print
extern castIntToString
extern exit
extern debug
min:

	push ebp
	mov ebp,esp ; Stack frame

	mov ebx, [ebp+8] ; puntero al array
	mov ecx, [ebp+12] ; array length
	; Si es uno o 0 devuelvo 1
	cmp ecx, 0
	je return
	mov eax, [ebx] ; en eax esta el valor minimo, agarrando el primer valor	
minCiclo:
	add ebx,4
	dec ecx
	cmp ecx, 0
	je printResult
	mov edx,[ebx]
	push edx
	cmp edx,eax
	jg minCiclo
	mov eax,edx
	jmp minCiclo
printResult:
	push eax
	; call debug
	call castIntToString
	add esp,4
	push ebx
	push eax
	call _print
	add esp,8
return:
	mov esp,ebp
	pop ebp
	ret

ret1:
	mov eax,1
	jp return


