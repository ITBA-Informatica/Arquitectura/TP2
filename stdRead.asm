sys_read  equ 3
sys_write equ 4
stdout    equ 1
stdin     equ 0

section .text

global stdRead
extern printInt
extern toupper
extern printN
extern strlen
extern exit

stdRead:

pushad
mov eax,sys_read
mov ebx,stdin
mov ecx,buffer
mov edx,1000
int 80h
mov ebx, buffer
add ebx,eax
dec ebx
mov ecx,0
mov [ebx],ecx
popad
mov eax,buffer
ret

section .bss
        buffer resb 128