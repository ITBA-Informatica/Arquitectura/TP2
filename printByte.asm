section .text

global printByte
extern castByteToString
extern printN
extern print
extern exit

printByte:
	push ebp
	mov ebp,esp ; Stack frame
	pushad
	mov ax,0
	mov al, [ebp+8]
	push ax
	call castByteToString
	inc esp
	push eax
	call printN
	add esp,4
	popad
	mov esp,ebp
	pop ebp
	ret
