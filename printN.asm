section .text
GLOBAL printN
extern print
extern printNewLine

; Funcion print:
; pushear al stack en orden: 
;  - length
;  - string
; 
; 
;Example:  
;_start:
;	push length
;	push string
;	call _print
;	mov eax,0x01
;	int 80h

printN:
; Levanto length y string a edx y ecx y devuelvo el return address
	push ebp
	mov ebp,esp ; Stack frame
	pushad

	mov eax,[ebp+8]
	push eax
	call print
	add esp,4	
	call printNewLine

	popad
	mov esp,ebp
	pop ebp
	ret
