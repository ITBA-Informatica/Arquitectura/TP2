section .text

global strlen
extern exit
extern printInt
extern print
strlen:	
	push ebp
	mov ebp,esp ; Stack frame

	sub esp,4 ; reservo 4 bytes
	pushad

	mov ebx,[ebp+8] ; pointer al string
	mov edx, 0
ciclo:
	inc edx
	inc ebx
	mov cl,[ebx]	
	cmp cl,0
	jnz ciclo
	mov [ebp], edx
	popad
	
	
	mov eax,[ebp]

	mov esp,ebp
	pop ebp
	ret

