section .text
GLOBAL _start
extern _print
extern toupper

_start:
        ;cargo parametros y llamo a toupper
        push length
        push string
        call toupper
        ;Mando a imprimir lo q volvio desde el stack
        call _print

        ;return
        mov eax,1
        int 80h

section .data
string db "h4ppy c0d1ng"
length equ $-string
