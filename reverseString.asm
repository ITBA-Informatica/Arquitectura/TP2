section .text
GLOBAL reverseString
extern _print
extern exit

reverseString:

	push ebp
	mov ebp, esp ;Stackframe
	
    mov ecx, [ebp+8] ; string
    mov edx, [ebp+12] ; length
    mov eax,edx
    add ecx,edx
    dec ecx ; ecx apunta al ulitmo valor
	mov ebx, reversedString

cic:
	mov eax, [ecx]
	mov	[ebx], eax
	inc ebx
	dec ecx
	dec edx
	jnz cic

	mov eax,reversedString ; reversed string pointer
	mov ebx,[ebp+12] ; length

	mov esp,ebp
	pop ebp
	ret




section .bss
        reversedString resb 128