section .text
GLOBAL sumToN

sumToN:
	pop eax; returnaddress
	pop ebx; number
	push eax
	push ebx; Stack = ReturnAdddress | number

	cmp ebx,1
	je returnBase; Caso base
	dec ebx ; Si no estoy en el caso base, llamo recursivamente a n-1  y me gaurdo en el stack el valor q tenia
	push ebx; llamo con n-1
	call sumToN;
	pop ebx; Respuesta recursiva
	pop eax; Valor de la iteracion actual
	pop ecx; return address
	add eax,ebx
	push eax ; cargo la respuesta en el stack y vuelvo
	push ecx
	ret
	
returnBase:
	; Cargo la direccion de retorno en el stack y la devuelvo
	pop ebx ; numero (1)
	pop eax ; returnAddress
	push ebx
	push eax
	ret