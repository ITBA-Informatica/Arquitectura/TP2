section .text
GLOBAL printNewLine

; Funcion print:
; pushear al stack en orden: 
;  - length
;  - string
; 
; 
;Example:  
;_start:
;	push length
;	push string
;	call _print
;	mov eax,0x01
;	int 80h

printNewLine:
; Levanto length y string a edx y ecx y devuelvo el return address
	pushad
	mov edx,1
	mov ecx,newLine
	mov eax,4
	mov ebx,1
	int 80h
	popad
	ret
	
section .data
newLine db 10