section .text

global _start	
extern _print
extern sortArray
extern printArray
extern castIntToString
extern exit
extern debug
_start:

mov eax,[length]
push eax
push array
call sortArray
call exit

section .data
array:
	dd 10
	dd 8
	dd 1
	dd 5
	dd 2
	dd 21
	dd 3
length: db 7
