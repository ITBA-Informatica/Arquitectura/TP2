section .text


global printStringArray
extern exit
extern printInt
extern printN
extern print


printStringArray:





	push ebp
	mov ebp, esp
    pushad

    mov eax,[ebp+12] ; Array length
    mov ebx,[ebp+8] ; stringArray
ciclo:
    ; print
    mov ecx,[ebx]
    push ecx
    call print
    add esp,4
    add ebx,4
    dec eax
    jnz ciclo

    popad
    mov esp,ebp
    pop ebp