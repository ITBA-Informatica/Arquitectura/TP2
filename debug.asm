section .text

global debug
extern castIntToString
extern _print

debug:
	pushad
	push eax
	call castIntToString
	add esp,4
	push ebx
	push eax
	call _print
	add esp,8
	popad
	ret