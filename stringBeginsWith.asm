section .text

global stringBeginsWith
extern exit
extern printN
extern printInt

stringBeginsWith:
push ebp
mov ebp,esp

sub esp,8;reservo para la respuesta
pushad

mov ebx,[ebp+8]; beginString
mov ecx,[ebp+12]; stringToCheck


add esp,4
ciclo:
mov eax,0
mov al,[ebx]
cmp eax,0
je returnTrue; termino el string
mov edx,0
mov dl,[ecx]
cmp dl,0
je returnFalse
cmp al,dl
jne returnFalse
inc ebx
inc ecx
jmp ciclo
returnTrue:
mov eax,1
mov [ebp-4],eax
jmp return
returnFalse:
mov eax,0
mov [ebp-4],eax
jmp return

return:
popad
pop eax
mov ebp,esp
pop ebp
ret