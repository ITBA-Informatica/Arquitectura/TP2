# Libreria:

## print

- recibe en stack: 
	- length 
	- string
	- returnaddress ( que viene del call)

- devuelve:
	- nada


## reverseString

- recibe en stack: 
	- length 
	- string
	- returnaddress ( que viene del call)

- devuelve:
	- length
	- string

## castIntToString

- recibe en stack: 
	- int
	- returnaddress ( que viene del call)

- devuelve:
	- length
	- string

## toupper

- recibe en stack: 
	- length 
	- string
	- returnaddress ( que viene del call)

- devuelve:
	- length
	- string

## sumToN
- recibe en stack:
	- int
	- returnaddress ( que viene del call)
- devuelve:
	- int (suma de 1 a n del int recibido)