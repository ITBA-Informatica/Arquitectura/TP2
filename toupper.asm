section .text
GLOBAL toupper
extern printInt

toupper:
        mov ebp,esp
        pushad
        mov edx,[ebp+8]; edx == length
        mov ecx,[ebp+4]; ecx == string
ciclo:
        mov byte al,[ecx]               ; valor del string en eax
        cmp al,97                       ; lo comparo con a
        jl next                         ; si es mas chico, sigo
        cmp al,122                      ; comparo con z
        jg next                         ; si es mas grande, sigo
        sub al,20h              ; si no segui, le resto 32 para hacerlo uppercase
        mov [ecx], al
next:
        inc ecx
        dec edx
        jnz ciclo
        popad
        ret