section .text

global printInt
extern castIntToString
extern printN
extern print

printInt:
	push ebp
	mov ebp,esp ; Stack frame
	pushad

	mov eax, [ebp+8]
	push eax
	call castIntToString
	add esp,4
	push eax
	call printN
	add esp,4
	popad
	mov esp,ebp
	pop ebp
	ret
