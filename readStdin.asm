section .text

global _start
extern printInt
extern toupper
extern printN
extern strlen
extern stdRead
extern exit

_start:

call stdRead
push eax
call strlen
push eax
mov eax,[esp+4]
push eax
call toupper
add esp,8
push eax
call printN
; add esp,4
; push eax
; call printN

call exit
section .data
start db "Start!", 0
