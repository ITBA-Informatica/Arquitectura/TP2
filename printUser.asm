section .text

global _start
extern printN
extern printInt
extern exit
extern printN
extern castIntToString
extern printStringArray
extern stringBeginsWith
_start:

mov ebp,esp

mov eax,[ebp]
add eax,2

mov ecx,4
mul ecx
mov ebx,ebp
add ebx,eax
; Pase todas las variables de parametros, ebx apunta a la primer variable de entorno
ciclo:
push ebx
mov eax,[ebx]
push eax
push string
call stringBeginsWith
add esp,8
pop ebx
add ebx,4
cmp eax,1
jne ciclo

sub ebx,4
mov eax,[ebx]
push eax
call printN
add esp,4

call exit

section .data

string db "USER=", 0
true db "true", 0
false db "false", 0