section .text

global _start
extern _print
extern factorial
extern castIntToString
extern exit
_start:

push 5
call factorial
push eax
call castIntToString
add esp,4
push ebx
push eax
call _print
call exit