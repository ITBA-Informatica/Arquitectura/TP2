section .text
extern printN
extern printInt
GLOBAL castIntToString
extern reverseString
extern exit

; Recibe del stack un string y su length (primero length y despues string)
; Devuelve el length y el puntero al string en uppercase (convierte el string existente)

castIntToString:

        push ebp
        mov ebp, esp

        sub esp,8 ; reservo para la respuesta
        pushad

        mov eax,[ebp+8]
        mov ebx,reversedString
        ;0 en edx
        ;251 en eax
        ;ebx va a ser el length del string
        ; Div op  ----    Eax = edx:eax / Op ; Resto en edx---- 
        ;return
        
ciclo:
        mov edx,0
        mov ecx, 10
        div ecx ; Divido por 10
        add edx, 48 ; El resto lo agrego al string
        mov [ebx], edx
        inc ebx; Voy a la siguiente posicion de memoria
        cmp eax, 10
        jge ciclo
        add eax, 48
        mov [ebx], eax
        sub ebx, reversedString
        inc ebx
        push ebx
        push reversedString
        call reverseString
        add esp,8
; Le agrego un 0 al final del string
        mov ecx,eax
        add ecx,ebx
        mov edx,0
        mov [ecx], edx

        mov [ebp],eax
        mov [ebp-4],ebx
        popad
        mov eax, [ebp]
        mov ebx, [ebp-4]

        mov esp,ebp
        pop ebp
        ret

section .bss
        reversedString resb 128