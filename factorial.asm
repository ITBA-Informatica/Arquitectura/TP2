section .text

global factorial

factorial:

	push ebp
	mov ebp,esp ; Stack frame

	mov ebx, [ebp+8]

	; Si es uno o 0 devuelvo 1
	cmp ebx, 0
	je ret1
	cmp ebx,1
	je ret1
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	; Sino, llamo recursivamente a factorial de n-1
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	dec ebx
	push ebx
	call factorial ; eax = factorial n-1
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	add esp,4 ; limpio el stack
	mov ebx,[ebp+8]
	mov edx,0
	mul ebx ;multiplico por la respuesta 
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
return:
	mov esp,ebp
	pop ebp
	ret

ret1:
	mov eax,1
	jp return
	