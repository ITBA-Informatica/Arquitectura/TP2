section .text

global _start
extern _print
extern printArray
extern castIntToString
extern exit
_start:

push length
push array
call printArray
call exit

section .data
string db "Hello world"
array:
dd 15    	; Move the 32-bit integer representation of 2 into the 4 bytes starting at the address in EBX.db 4 
dd 8
dd 12
dd 5
dd 4
dd 3
dd 20
length: db 4
