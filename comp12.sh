nasm -f elf printArguments.asm 
nasm -f elf printArrayPointer.asm 
nasm -f elf printInt.asm 
nasm -f elf castIntToString.asm 
nasm -f elf reverseString.asm 
nasm -f elf print.asm 
nasm -f elf printN.asm 
nasm -f elf exit.asm 
nasm -f elf stringBeginsWith.asm 
nasm -f elf printNewLine.asm 
nasm -f elf printUser.asm 
nasm -f elf strlen.asm 
nasm -f elf memDump.asm 
nasm -f elf memDumpTest.asm 
nasm -f elf castByteToString.asm
nasm -f elf printByte.asm
ld -m elf_i386 exit.o strlen.o memDump.o memDumpTest.o stringBeginsWith.o  printNewLine.o print.o reverseString.o printN.o printInt.o castIntToString.o castByteToString.o printByte.o -o memDump

./memDump