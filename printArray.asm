section .text

global printArray
extern castIntToString
extern exit
extern debug
extern printS
extern printInt
extern printNewLine
printArray:

	push ebp
	mov ebp,esp ; Stack frame
	pushad

	mov ebx, [ebp+8] ; puntero al array
	mov ecx, [ebp+12] ; length
	; Si es uno o 0 devuelvo 1
	cmp ecx, 0
	je return
ciclo:
	mov eax,[ebx]
	push eax
	call printInt
	add esp,4
	add ebx,4
	dec ecx
	jnz ciclo
	call printNewLine
return:
	popad
	mov esp,ebp
	pop ebp
	ret

ret1:
	mov eax,1
	jp return


