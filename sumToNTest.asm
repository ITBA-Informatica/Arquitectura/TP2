section .text
GLOBAL _start
extern _print
extern sumToN
extern castIntToString

_start:
	mov eax, 6
	push eax
	call sumToN
	call castIntToString
	call _print
	mov eax,0x01
	int 80h

