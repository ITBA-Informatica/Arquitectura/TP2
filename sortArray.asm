section .text
global sortArray
extern debug
extern printArray
extern exit
extern printInt
extern printNewLine

sortArray:

;;;; ebp + 8 = array
;;;; ebp +12 = length
	push ebp
	mov ebp,esp ; Stackframe
	pushad

	mov edx,0 ; i
	push edx
	push edx
fori:
	pop ecx	
	mov ecx,0
	push ecx
forj:
	mov edx,[ebp+8]

	add edx,ecx
	mov eax,[edx] ; cargo a con a[i]
	mov ebx,[edx+4] ; cargo b con a[i+1]
	cmp eax,ebx
	jl next
switch:
	mov [edx], ebx
	mov [edx+4], eax
next:	
	add ecx,4
	pop ebx
	inc ebx
	push ebx
	mov eax,[ebp+12]
	dec eax
	cmp ebx,eax
	jl forj
nextJ:
	mov eax,[ebp+12]
	mov ebx,[esp+4] ; i
	inc ebx
	mov [esp+4], ebx ; i
	cmp ebx,eax
	jl fori
	call debugArray
	call exit
return:
	popad
	mov esp,ebp
	pop ebp
	ret

debugArray:
	pushad
	mov eax,[ebp+8]
	mov ebx,[ebp+12]
	push ebx
	push eax
	call printArray
	add esp,8
	popad
	ret
